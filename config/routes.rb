Rails.application.routes.draw do
  #get 'home/index'

  root to: 'home#index'
  resources :home, only: [:index]
  get 'get_text' => "home#get_text"


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
